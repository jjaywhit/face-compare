import base64
import http.server
import json
import time
import urllib

HOST_NAME = 'localhost'
PORT_NUMBER = 8080
SRC_DIR = './back-end'
FACE_COMP_DIR = '~/root/openface/demos/'


class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()

    def do_POST(s):
        s.do_HEAD()
        ctype = s.getHeader(s.headers._headers, 'content-type')
        if(ctype[1].lower() == "application/json; charset=utf-8"):
            content_length = int(s.headers['Content-Length'])
            body = s.rfile.read(content_length)
            photos = json.loads(body)
            for p in photos:
                file = open(SRC_DIR + '/' + p.get('name') + '.' + s.parseType(p.get('type')), "wb+")
                if(p.get('base') == 64):
                    file.write(base64.b64decode(p.get('content')))
                file.close()

        s.wfile.write(bytearray("thanks for the post", 'utf8'))

    def do_GET(s):
        """Respond to a GET request."""
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write(bytearray("<html><head><title>Title goes here.</title></head>", 'utf8'))
        s.wfile.write(bytearray("<body><p>This is a test.</p>", 'utf8'))
        # If someone went to "http://something.somewhere.net/foo/bar/",
        # then s.path equals "/foo/bar/".
        s.wfile.write(bytearray("<p>You accessed path: %s</p>" % s.path, 'utf8'))
        s.wfile.write(bytearray("</body></html>", 'utf8'))

    def do_OPTIONS(s):
        s.send_response(200, "ok")
        s.send_header('Access-Control-Allow-Origin', '*')
        s.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        s.send_header("Access-Control-Allow-Headers", "X-Requested-With")
        s.end_headers()

    def parseType(s, type):
        t = 'jpg'
        if(type.lower() == 'image/png'):
            t ='png'
        return t

    def getHeader(s, headers, header):
        for i in range(len(headers)):
            if((headers[i])[0].lower() == header):
                return headers[i]


def run():
    server_class = http.server.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print(time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))


if __name__ == '__main__':
    run()
