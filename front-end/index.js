class Photo {
    constructor(content, name, type) {
        this.base = 64;
        this.content = content;
        this.name = name;
        this.type = type;
    }
}

var photos = [];

function readURL(input, imgNum) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var BASE64_MARKER = ';base64,';
            var DATA_MARKER = 'data:';
            var content;
            var name;
            var type;
            var parts;
            var result = e.target.result;

            if(imgNum == 0){
                $('#selfImg').attr('src', result).width(150).height(200);
                name = 'self';
            }
            else if(imgNum == 1){
                $('#momImg').attr('src', result).width(150).height(200);
                name = 'mom';
            }
            else if(imgNum == 2){
                $('#dadImg').attr('src', result).width(150).height(200);
                name = 'dad';
            }

            parts = result.split(BASE64_MARKER);
            type = parts[0].split(DATA_MARKER)[1];
            content = parts[1];
            photos.push(new Photo(content, name, type));
        };

        reader.readAsDataURL(input.files[0]);

    }
}

function send() {

    //if(photos.size == 3) {
        var contentLength = 9090;
        var body = JSON.stringify(photos);
        $.ajax({
            url: 'http://localhost:8080/compare',
            data: body,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        }).success(function(r) {
            console.log(r);
        });
    //}
    /*else {
        alert('Fill a fields');
    }*/
}